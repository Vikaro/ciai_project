package ciai;

import ciai.model.Course;
import ciai.model.Repository.UserRepository;
import ciai.model.User;
import ciai.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import java.util.ArrayList;

/**
 * Created by stili on 12/17/2016.
 */

@Controller
@RequestMapping(value = "/users")
@SessionAttributes("loggedUser")
public class UserController {

    @Autowired
    UserRepository users;

    @RequestMapping(value = { "/all", ""})
    public @ResponseBody
    @JsonView(Views.StudentView.class)
    Iterable<User> getUsers(){
        return users.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @JsonView(Views.StudentView.class)
    public @ResponseBody User getUser(@PathVariable long id) {
        return users.findOne(id);
    }

    @RequestMapping(value = "/register/", method = RequestMethod.POST)
    @JsonView(Views.Public.class)
    public @ResponseBody User register(
            @RequestParam String login,
            @RequestParam String password,
            @RequestParam String repeat) {

        ModelAndView modelAndView = new ModelAndView();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        User newUser = new User(login, encoder.encode(password), "", "", "","","", new ArrayList<Course>(), "student");
        users.save(newUser);
        modelAndView.addObject("loggedUser", newUser);
        return null;
    }

    @RequestMapping(value = "/login/", method=RequestMethod.POST)
    public @ResponseBody String login(@RequestParam String login,
                                    @RequestParam String password) {
        List<User> userList = users.findByLogin(login);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        if (userList.size() ==0) {
            return null;
        }
        for (User u : userList) {
            if(encoder.matches(password, u.getPassword())){
                if (u.getRole().equals("professor")) {
                    return "/#/professor/" + u.getId()+ "/";
                } else if (u.getRole().equals("professor_assistant")) {
                    return "/#/assistant/" + u.getId()+ "/";
                } else {
                    return "/#/student/" + u.getId() + "/";
                }
            }
        }
        return null;
    }

    @RequestMapping(value= {"", "/addStudent", "/addProfessor"}, method=RequestMethod.POST)
    public @ResponseBody long addCourse(@RequestBody User new_user) {
        users.save(new_user);
        return new_user.getId();
    }

    @RequestMapping(value = "/addUsers", method = RequestMethod.POST)
    public ResponseEntity<List<User>> addCourses(@RequestBody List<User> new_students) {
        // TODO: call persistence layer to update
        new_students.forEach(user -> users.save(user));
        return new ResponseEntity<List<User>>(new_students, HttpStatus.OK);
    }


    @RequestMapping(value = "/update/", method=RequestMethod.POST)
    public @ResponseBody String update(
            @RequestParam String id,
            @RequestParam String name,
            @RequestParam String emailPrivate,
            @RequestParam String emailUni) {

        User user = users.findOne( Long.parseLong(id) );
        user.setName(name);
        user.setEmailPrivate(emailPrivate);
        user.setEmailUni(emailUni);
        users.save(user);
        return null;
    }

}
