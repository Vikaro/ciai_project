package ciai.model;

import ciai.views.Views;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;

@Entity
public class EvaluationStep {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonView(Views.Public.class)
    private long id;

    @JsonView(Views.Public.class)
    private String name;


    public EvaluationStep() {
    }

    public EvaluationStep(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long evaluation_id) {
        this.id = evaluation_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
