package ciai.model.oldModels;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

//public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {
//
//    List<Student> findByName(String name);
//
//    List<Student> findByAge(int age);
//
//    List<Student> findByAgeGreaterThan(int age);
//
//    @Query("select s from Student s where s.name like CONCAT(?,'%')")
//    List<Student> search(String name);
//
//    @Query(" select s from Student s "+
//     	   "   inner join s.courses en "+
//     	   " where en.course.id = :id")
//     List<Student> findByCourse(@Param("id") long id);
//
//    @Query(" select s from Student s "+
//    	   "   inner join s.courses en "+
//    	   "   inner join en.course c "+
//    	   " where c.name = :name")
//    List<Student> searchByCourse(@Param("name") String name);
//
////    @Query("select new ciai.model.StudentSummary(s.name,s.age) from Student s")
////    List<StudentSummary> summaryStudents();
//
//    Page<Student> findByName(String name, Pageable pageable);
//}
