package ciai.model;

import ciai.views.Views;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;

/**
 * Created by Bajkow on 17.12.2016.
 */
@Entity
public class Grade {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonView(Views.Public.class)
    private Integer id;
    @JsonView(Views.Public.class)
    private Integer grade;
    @OneToOne
    @JsonView(Views.Public.class)
    private EvaluationStep evaluationStep;
    @OneToOne
    @JsonView(Views.Public.class)
    private User user;

    public Grade() {
    }

    public Grade(Integer grade, EvaluationStep evaluationStep, User user) {
        this.grade = grade;
        this.evaluationStep = evaluationStep;
        this.user = user;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public EvaluationStep getEvaluationStep() {
        return evaluationStep;
    }

    public void setEvaluationStep(EvaluationStep evaluationStep) {
        this.evaluationStep = evaluationStep;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
