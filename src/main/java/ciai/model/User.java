package ciai.model;

import ciai.views.Views;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonView(Views.Public.class)
    private long id;
    @JsonView(Views.UserPrivateView.class)
    private String login;

    @JsonView(Views.UserPrivateView.class)
    private String password;
    @JsonView(Views.Public.class)
    private String name;
    @JsonView(Views.UserPrivateView.class)
	private String emailPrivate;
    @JsonView(Views.Public.class)
    private String emailUni;
    @JsonView(Views.Public.class)
    private String studentNumber;
    @JsonView(Views.Public.class)
    private String departament;

//    @ManyToMany(mappedBy = "users", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ManyToMany(mappedBy = "users")
    @JsonView(Views.StudentView.class)
    private Collection<Course> courses;

	//private int role;


    private void setUserData(String login, String password, String name, String emailPrivate, String emailUni, String role){
        this.login = login;
        this.password = password;
        this.name = name;
        this.emailPrivate = emailPrivate;
        this.emailUni = emailUni;
        this.role = role;
    }

    public User() {
    }

    public User(String login, String password, String name, String emailPrivate, String emailUni, String studentNumber, String departament, Collection<Course> courses,  String role) {
        this.setUserData(login, password, name, emailPrivate, emailUni, role);
        this.studentNumber = studentNumber;
        this.departament = departament;
        this.courses = courses;
    }

    public User(String login, String password, String name, String emailPrivate, String emailUni, String role) {
        setUserData(login, password, name, role, emailPrivate, emailUni);
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailPrivate() {
        return emailPrivate;
    }

    public void setEmailPrivate(String emailPrivate) {
        this.emailPrivate = emailPrivate;
    }

    public String getEmailUni() {
        return emailUni;
    }

    public void setEmailUni(String emailUni) {
        this.emailUni = emailUni;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getDepartament() {
        return departament;
    }

    public void setDepartament(String departament) {
        this.departament = departament;
    }

    public Collection<Course> getCourses() {
        return courses;
    }

    public void setCourses(Collection<Course> courses) {
        this.courses = courses;
    }

    public void addCourse(Course course){
        if(courses == null){
            courses = new ArrayList<Course>();
        }
        courses.add(course);
    }

    @JsonView(Views.Public.class)
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
