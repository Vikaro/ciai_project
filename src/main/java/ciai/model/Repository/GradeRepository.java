package ciai.model.Repository;

import ciai.model.Course;
import ciai.model.EvaluationStep;
import ciai.model.Grade;
import ciai.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Mateusz on 16.12.2016.
 */
public interface GradeRepository extends CrudRepository<Grade, Long> {

    List<Grade> findByUser(User user);
//    List<Grade> findByCourse(Course course);
    List<Grade> findByEvaluationStepAndUser(EvaluationStep step, User user);
}
