package ciai.model.Repository;

import ciai.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Mateusz on 16.12.2016.
 */
public interface UserRepository extends CrudRepository<User, Long> {
   List<User> findByName(String name);
   List<User> findByLogin(String login);
   User findById(long id);
}
