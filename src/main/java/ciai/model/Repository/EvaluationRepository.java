package ciai.model.Repository;

import ciai.model.EvaluationStep;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Mateusz on 16.12.2016.
 */
public interface EvaluationRepository extends CrudRepository<EvaluationStep, Long> {

}
