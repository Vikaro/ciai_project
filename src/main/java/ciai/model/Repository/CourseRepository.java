package ciai.model.Repository;

import ciai.model.Course;
import ciai.model.EvaluationStep;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Mateusz on 16.12.2016.
 */
public interface CourseRepository extends CrudRepository<Course, Long> {
    @Override
    List<Course> findAll();

    List<Course> findByName(String name);

    Course findById(long id);
}
