package ciai.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.cglib.core.Predicate;

import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Course {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonView(Views.Public.class)
    private long id;

    @JsonView(Views.Public.class)
    private int courseNumber;

    @JsonView(Views.Public.class)
    private String name;

    @ManyToMany
    @JsonView(Views.CourseView.class)
    private Collection<User> users;

    @JsonView(Views.Public.class)
    private String description;

    @JsonView(Views.Public.class)
    private int ects;

    @JsonView(Views.Public.class)
    private int edition;

    @OneToMany
    @JsonView(Views.Public.class)
    private Collection<EvaluationStep> evaluationSteps;

    @JsonView(Views.Public.class)
    private String years;


    public Course(int courseNumber, String name, String description, int ects, int edition, String years) {
        this.courseNumber = courseNumber;
        this.name = name;
        this.description = description;
        this.ects = ects;
        this.edition = edition;
        this.users = new ArrayList<User>();
        this.evaluationSteps = new ArrayList<EvaluationStep>();
        this.years = years;
    }


    public Course() {
    }

    public long getId() {
        return id;
    }

    public void setId(long course_id) {
        this.id = course_id;
    }

    public int getCourseNumber() {
        return courseNumber;
    }

    public void setCourseNumber(int course_number) {
        this.courseNumber = course_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getEcts() {
        return ects;
    }

    public void setEcts(int ects) {
        this.ects = ects;
    }

    public int getEdition() {
        return edition;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }

    public Collection<User> getStudents() {
        return users;
    }

    public void setStudents(Collection<User> students) {
        this.users = students;
    }

    public void addStudent(User student){
        System.out.println(users);
        if(users == null){
            users = new ArrayList<User>();
        }
        users.add(student);
    }
    public Collection<EvaluationStep> getEvaluationSteps() {
        return evaluationSteps;
    }

    public void setEvaluationSteps(Collection<EvaluationStep> evaluationSteps) {
        this.evaluationSteps = evaluationSteps;
    }
    public void addEvaluation(EvaluationStep step){
        System.out.println(evaluationSteps);
        if(evaluationSteps == null){
            evaluationSteps = new ArrayList<EvaluationStep>();
        }
        evaluationSteps.add(step);
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }
}
