package ciai;

import ciai.model.Course;
import ciai.model.EvaluationStep;
import ciai.model.Grade;
import ciai.model.Repository.*;
import ciai.model.User;
//import ciai.model.User_Professor;
//import ciai.model.User_Student;
//import ciai.model.oldModels.Student;
//import ciai.model.oldModels.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
//
//import ciai.model.oldModels.CourseRepository;
//import ciai.model.oldModels.ProfessorRepository;
//import ciai.model.oldModels.Student;
//import ciai.model.oldModels.StudentRepository;
//import ciai.model.oldModels.UserRepository;
//import ciai.services.ProfessorService;

@SpringBootApplication
@EnableSpringDataWebSupport
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ClassroomApplication {

	private static final Logger log = LoggerFactory.getLogger(ClassroomApplication.class);
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public static void main(String[] args) {
		SpringApplication.run(ClassroomApplication.class, args);
//		HibernatePersistenceProvider persistanceProvider = new HibernatePersistenceProvider();
//		EntityManagerFactory entityManagerFactory = persistanceProvider.
//				createEntityManagerFactory("NewPersistanceUnit", new HashMap());
//		EntityManager entityManager = entityManagerFactory.createEntityManager();
//
//		entityManager.close();
	}

	@Bean
	public CommandLineRunner init(
            CourseRepository courses,
//            User_ProfessorRepository professors,
//            User_StudentRepository students,
            EvaluationRepository evaluations,
//            Student_CourseRepository student_courses,
            GradeRepository grades,
            UserRepository users
			) {

		return (args) -> {

			User u1 = new User("u1",encoder.encode("password"),"Joao","private@private.com",
					"u1@fct.com","1234","123", new ArrayList<Course>(), "student");
			User u2 = new User("u2",encoder.encode("password"),"aaa","eee@private.com",
					"u2@fct.com","12345","1234", new ArrayList<Course>(), "student");

            User p1 = new User("p1",encoder.encode("password"),"aaa","eee@private.com",
                    "u2@fct.com","12345","1234", new ArrayList<Course>(), "professor");
            users.save(u1);
            users.save(u2);
            users.save(p1);

            Course c1 = new Course(1,"CIAI","Description .....", 6,1,"2015/2016");
            c1.addStudent(u1);
            c1.addStudent(u2);
            c1.addStudent(p1);
            EvaluationStep e1 = new EvaluationStep("test1");
            evaluations.save(e1);
            c1.addEvaluation(e1);
            Course c2 = new Course(2,"IPM","Description IPM+", 3,1,"2016/2017");
            c2.addStudent(u2);
            courses.save(c1);
            courses.save(c2);

            Grade g1 = new Grade(2,e1,u1);
            grades.save(g1);


//            users.save(user1);
//            users.save(user2);
//            users.save(user3);
//            users.save(user4);
//            users.save(user5);
//
//            students.save(student1);
//            students.save(student2);
//            students.save(student3);
//
//            professors.save(professor1);
//            professors.save(professor2);
//
//            courses.save(new Course(1000, "CIAI", "CIAI", 6, 2016) );
//            courses.save(new Course(1001, "AA", "AA", 6, 2016) );
//            courses.save(new Course(1002, "CGI", "CGI", 6, 2016) );
//            courses.save(new Course(1003, "IPM", "IPM", 6, 2016) );


        };

//			for(Student s: students) repository.save(s);
//
//			log.info("Students found with findAll():");
//			log.info("-------------------------------");
//			for (Student ss: repository.findAll()) {
//				log.info(ss.toString());
//			}
//            log.info("");
//
//			// fetch an individual customer by ID
//			Student student = repository.findOne(1L);
//			log.info("Student found with findOne(1L):");
//			log.info("--------------------------------");
//			log.info(student.toString());
//            log.info("");
//
//			// fetch student by name
//			log.info("Student found with findByName('Johannes Kepler'):");
//			log.info("--------------------------------------------");
//			for (Student johannes : repository.findByName("Johannes Kepler")) {
//				log.info(johannes .toString());
//			}
//            log.info("");
//
//			// fetch student by age with greater than
//			log.info("Student found with findByAgeGreaterThan(20):");
//			log.info("--------------------------------------------");
//			for (Student older : repository.findByAgeGreaterThan(20)) {
//				log.info(older.toString());
//			}
//            log.info("");
//
//			// search student by name
//			log.info("Student found with search('J'):");
//			log.info("--------------------------------------------");
//			for (Student namedJo : repository.search("J")) {
//				log.info(namedJo.toString());
//			}
//            log.info("");
//
//            Course[] courses = { new Course("AED",6),
//            		             new Course("CIAI",6),
//            		             new Course("ICL",6) };
//
//			for(Course c: courses) courseRepository.save(c);
//
//			log.info("Courses found with findAll():");
//			log.info("-------------------------------");
//			for (Course cc: courseRepository.findAll()) {
//				log.info(cc.toString());
//			}
//            log.info("");
//
//			log.info("Updating course name 'AED' to 'AED1':");
//			log.info("-------------------------------");
//			Course aed = courseRepository.findByName("AED").get(0);
//			aed.setName("AED1");
//			courseRepository.save(aed);
//
//			log.info("Enrolling students in 'AED1':");
//			log.info("-------------------------------");
//			for(Student ss:repository.findByAgeGreaterThan(20))
//			aed.getEnrollments().add(new Enrollment(aed,ss));
//			courseRepository.save(aed);
//            log.info("");
//
//			log.info("Enrolling students in 'ICL':");
//			log.info("-------------------------------");
//			Course icl = courseRepository.findByName("ICL").get(0);
//			for(Student ss:repository.findByAgeGreaterThan(20))
//			icl.getEnrollments().add(new Enrollment(icl,ss));
//			courseRepository.save(icl);
//            log.info("");
//
//            log.info("Courses found with findAll():");
//			log.info("-------------------------------");
//			for (Course cc: courseRepository.findAll()) {
//				log.info(cc.toString());
//				log.info("-------------------------------");
//				for(Enrollment e: cc.getEnrollments())
//					log.info(e.toString());
//				log.info("-------------------------------");
//				log.info("");
//			}
//            log.info("");
//
//            log.info("Courses found with findAll():");
//			log.info("-------------------------------");
//			for (Student s: repository.findAll()) {
//				log.info(s.toString());
//				log.info("-------------------------------");
//				for(Enrollment e: s.getCourses())
//					log.info(e.getCourse().toString());
//				log.info("-------------------------------");
//				log.info("");
//			}
//            log.info("");
//
//            log.info("Students found with findByCourse(id):");
//			log.info("-------------------------------");
//			//Course c = courseRepository.findOne(1L);
//			for (Student s: repository.findByCourse(1L)) {
//				log.info(s.toString());
//			}
//			log.info("-------------------------------");
//            log.info("");
//
//            Professor profs[] = {new Professor("João Costa Seco"),
//            		             new Professor("Jácome Cunha")};
//
//            for(Professor p: profs) professors.save(p);
//
//			log.info("Adding courses to professors");
//			log.info("-------------------------------");
//
//            profService.addCourses("João Costa Seco", "ICL", "CIAI");
//            profService.addCourses("Jácome Cunha", "AED1", "CIAI");
//
//            log.info("Professors found with findByAll(id):");
//			log.info("-------------------------------");
//            for(Professor p: professors.findAll()) {
//            	log.info(p.getName());
//    			log.info("-------------------------------");
//            	for(String c: profService.getCourses(p.getId()))
//            		log.info(c);
//            }
//			log.info("-------------------------------");
//            log.info("");
//
//            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//
//            User[] us = {new User("Joao",encoder.encode("CostaSeco")),
//            			 new User("Jacome", encoder.encode("Cunha")) };
//
//            for(User u: us) users.save(u);

		};


}
