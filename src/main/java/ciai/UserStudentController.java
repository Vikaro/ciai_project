package ciai;

//import ciai.model.User_Student;
import ciai.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

///**
// * Created by stili on 12/17/2016.
// */
//
//@Controller
//@RequestMapping(value="/students")
//public class UserStudentController {
//
//        @Autowired
//        User_StudentRepository students;
//
//        @RequestMapping(value = { "/all", ""})
//        public @ResponseBody
//        Iterable<User_Student> getStudents() {
//            return students.findAll();
//        }
//
//        @RequestMapping(value = "/{id}", method = RequestMethod.GET)
//        @JsonView(Views.ExtendedCourseView.class)
//        public @ResponseBody User_Student getStudent(@PathVariable long id) {
//            User_Student student = students.findOne(id);
//            Hibernate.initialize(student.getDegree());
//            Hibernate.initialize(student.getStudent_number());
//            Hibernate.initialize(student.getUser_student_id());
//            return student;
//        }
//
//
//    }
