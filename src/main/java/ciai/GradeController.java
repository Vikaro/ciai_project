package ciai;

import ciai.model.Course;
import ciai.model.EvaluationStep;
import ciai.model.Grade;
import ciai.model.Repository.CourseRepository;
import ciai.model.Repository.GradeRepository;
import ciai.model.Repository.UserRepository;
import ciai.model.User;
import ciai.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Mateusz on 19.12.2016.
 */

@Controller
@RequestMapping(value = "/grades")
public class GradeController {

    @Autowired
    GradeRepository grades;

    @Autowired
    UserRepository users;

    @Autowired
    CourseRepository courses;
    @RequestMapping(value = "/{courseId}/{userId}", method = RequestMethod.GET)
    @JsonView(Views.Public.class)
    public
    @ResponseBody
    Iterable getCourseInformation(@PathVariable long courseId,@PathVariable long userId) {
        User user = users.findById(userId);
        Course course = courses.findById(courseId);
        Collection studentGrades = new ArrayList();
        for (EvaluationStep e: course.getEvaluationSteps()) {
            studentGrades.addAll(grades.findByEvaluationStepAndUser(e,user));
        }
        return studentGrades;
    }
}