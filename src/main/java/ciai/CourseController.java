package ciai;
//
import ciai.model.Course;
import ciai.model.EvaluationStep;
import ciai.model.Grade;
import ciai.model.Repository.CourseRepository;
import ciai.model.Repository.EvaluationRepository;
import ciai.model.Repository.UserRepository;
import ciai.model.User;
import ciai.views.Views;
import com.fasterxml.jackson.annotation.JsonView;
import ognl.Evaluation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.cglib.core.Predicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping(value = "/courses")
public class CourseController {

    @Autowired
    CourseRepository courses;

    @RequestMapping(value = {"/all", ""})
    @JsonView(Views.CourseView.class)
    public
    @ResponseBody
    Iterable<Course> getCourses() {
        return courses.findAll();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @JsonView(Views.CourseView.class)
    public
    @ResponseBody
    Course getCourse(@PathVariable long id) {
        return courses.findOne(id);
    }


    @RequestMapping(value = "/information/{courseId}", method = RequestMethod.GET)
    @JsonView(Views.CourseView.class)
    public
    @ResponseBody
    Course getCourseInformation(@PathVariable long courseId) {
        Course course = courses.findOne(courseId);

        Collection<User> users = course.getStudents();

        Collection smallList = CollectionUtils.filter(users, o -> {
            User c = (User) o;
            return c.getRole().equals("professor");
        });
        course.setStudents(smallList);

        return course;
    }

    @Autowired
    UserRepository students;
    @Autowired
    EvaluationRepository evals;

    //    	@Transactional
//    	@AllowedToAddStudentToCourse
    @RequestMapping(value = "/{id}/enrollStudent", method = RequestMethod.POST)
    public
    @ResponseBody
    long addStudent(@PathVariable long id, @RequestBody User student) {
        Course course = courses.findOne(id);
        System.out.println(student);
        System.out.println(student.getCourses());
        student.addCourse(course);
//            student.getCourses().add(course);
        students.save(student);
        return course.getId();
    }

    @RequestMapping(value = "/{course_id}/enroll_{student_id}", method = RequestMethod.POST)
    public
    @ResponseBody
    long addStudent(@PathVariable long course_id, @PathVariable long student_id) {
        Course course = courses.findOne(course_id);
        User student = students.findOne(student_id);
        student.addCourse(course);
        course.addStudent(student);
        students.save(student);
        courses.save(course);
        return course.getId();
    }

    ////
    @RequestMapping(value = {"", "/addCourse"}, method = RequestMethod.POST)
    public
    @ResponseBody
    long addCourse(@RequestBody Course course) {
        courses.save(course);
        return course.getId();
    }

    @RequestMapping(value = "/addCourses", method = RequestMethod.POST)
    public ResponseEntity<List<Course>> addCourses(@RequestBody List<Course> new_courses) {
        // TODO: call persistence layer to update
        new_courses.forEach(course -> courses.save(course));
        return new ResponseEntity<List<Course>>(new_courses, HttpStatus.OK);
    }

    @RequestMapping(value = "/addEval", method = RequestMethod.POST)
    public ResponseEntity<String> addEval(@RequestParam String name, @RequestParam String courseId) {
        EvaluationStep eval = new EvaluationStep(name);
        evals.save(eval);
        Course course = courses.findOne(Long.parseLong(courseId));
        course.addEvaluation(eval);
        courses.save(course);
        return new ResponseEntity<>("ok", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/students", method = RequestMethod.GET)
    @JsonView(Views.CourseView.class)
    public
    @ResponseBody
    Course getEnrolledStudents(@PathVariable long id) {
        Course course = courses.findOne(id);
        Collection<User> connectedUsers = course.getStudents();
        Collection enrolledStudents = CollectionUtils.filter(connectedUsers, o -> {
            User c = (User) o;
            return c.getRole().equals("student");
        });
        course.setStudents(enrolledStudents);
        return course;
    }
}