package ciai.views;

public class Views {
	public interface Public {}
	public interface CourseView extends Public {}
	public interface StudentView extends Public {}
	public interface UserPrivateView extends StudentView {}
	public interface EvaluationView {}
	public interface ExtendedStudentView extends StudentView, CourseView {}
	public interface ExtendedCourseView extends CourseView, StudentView {}
}
