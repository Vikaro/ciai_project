/**
 * Created by Mateusz on 27.10.2016.
 */

var Login = React.createClass({

    getInitialState: function() {
        return{
            login : "",
            password:""
        };
    },

    sendLoginForm() {
        $.ajax({
            url: "/users/login/",
            type: "POST",
            data: {
                login: this.state.login,
                password: this.state.password
            },
            success: function (data) {
                if (!$.trim(data)) {
                    alert("There's no such user")
                } else {
                    location.href = "" + data
                }
            },
            error: function (data) {
                alert("fail");
            }
        });
    },


    handleChangeLogin(event) {
        this.setState({login: event.target.value});
    },
    handleChangePassword(event) {
        this.setState({password: event.target.value});
    },
    render(){
        console.log("LOGIN " + loggedUser);
        return(
            <div>
                <div className="col-md-4"></div>
                <div className="col-md-4">
                    <p>Login: </p>
                    <input type="text" className="form-control"  value={this.state.login} onChange={this.handleChangeLogin} />
                    <p>Password: </p>
                    <input type="password" className="form-control" value={this.state.password} onChange={this.handleChangePassword}/>
                    <Link onClick={() => { this.sendLoginForm() }} className="btn btn-primary btn-lg btn-block" >Login</Link>
                    <br/>
                    <p> If you haven't already created an account, click Register</p>
                    <Link to="/register" className="btn btn-primary btn-lg btn-block" >Register</Link>
                </div>
                <div className="col-md-4"></div>
            </div>
        )
    }
});