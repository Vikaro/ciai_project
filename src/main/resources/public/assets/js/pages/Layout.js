/**
 * Created by Mateusz on 27.10.2016.
 */


var Layout = React.createClass( {
    render :function() {

        const { location } = this.props;
        return (
            <div>

                {/*  <Nav location={location} /> */}

                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <h1>New USOS</h1>

                            {this.props.children}

                        </div>
                    </div>
                   <Footer/>
                </div>
            </div>

        );
    }
});