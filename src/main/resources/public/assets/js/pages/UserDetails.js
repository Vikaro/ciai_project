/**
 * Created by stili on 12/16/2016.
 */
var UserDetails = React.createClass( {
    getInitialState: function() {

        {console.log("-------")}
        {console.log("UserDetails")}
        {console.log("-------------")}
        return{
            id : this.props.student_detail.id,
            name : this.props.student_detail.name,
            emailPrivate: this.props.student_detail.emailPrivate,
            emailUni: this.props.student_detail.emailUni,
            studentNumber: this.props.student_detail.studentNumber,
            departament: this.props.student_detail.departament,
            role : this.props.student_detail.role,
            editMode: false
        };
    },

    handleChangeName(event) {
        this.setState({name: event.target.value});
    }, handleChangeEmailUni(event) {
        this.setState({emailUni: event.target.value});
    }, handleChangeEmailPriv(event) {
        this.setState({emailPrivate: event.target.value});
    },

    changeEditState() {
        this.setState({editMode: false});
    },

    // componentWillMount: function(){
    //     console.log('Component WILL MOUNT PROPS!');
    //     console.log(this.props.student_detail);
    //
    //     this.setState({
    //         name : this.props.student_detail.name,
    //         emailPrivate: this.props.student_detail.emailPrivate,
    //         emailUni: this.props.student_detail.emailUni,
    //         studentNumber: this.props.student_detail.studentNumber,
    //         departament: this.props.student_detail.departament
    //
    //     });
    // },
    componentWillReceiveProps: function (nextProps) {
        console.log('Component WILL RECIEVE PROPS!');
        console.log(nextProps.student_detail);

        this.setState({
            id : nextProps.student_detail.id,
            name : nextProps.student_detail.name,
            emailPrivate: nextProps.student_detail.emailPrivate,
            emailUni: nextProps.student_detail.emailUni,
            studentNumber: nextProps.student_detail.studentNumber,
            departament: nextProps.student_detail.departament,
            role : nextProps.student_detail.role,
            editMode: false
        });
    },

    saveEditedData: function () {
        $.ajax({
            url: "/users/update/",
            type: "POST",
            data: {
                id: "" + this.state.id,
                name: "" + this.state.name,
                emailPrivate: "" + this.state.emailPrivate,
                emailUni: "" + this.state.emailUni,
            },
            success: function (data) {
                console.log("success")
            },
            error: function (data) {
                console.log("error")
            }
        });
        this.changeEditState();
    },

    render: function () {
        console.log("STATE" + this.state);
        if (this.state.editMode == false) {
            return (
                <div>
                    <div className="col-md-4">
                        <img src="http://67.media.tumblr.com/77daf6e61a4d79c0759ab4ef83c9b643/tumblr_inline_o78eltLfkE1u1xff4_500.png" height={150} width={180}/>
                    </div>
                    <div className="col-md-8">
                        <p> Name:  {this.state.name} </p>
                        <p> Email Private:  {this.state.emailPrivate}</p>
                        <p> Email University:  {this.state.emailUni}</p>
                        {this.state.role == "student" ? (
                            <p> Number:  {this.state.studentNumber}</p>
                        ) : (
                            <p> Departament:  {this.state.departament}</p>
                        )}
                        <button onClick={() => { this.setState({editMode: true}) } } className="btn btn-default">Edit</button>
                    </div>
                </div>
            )
        } else {
            return (
                <div>
                    <div className="col-md-4">
                        <img src="http://67.media.tumblr.com/77daf6e61a4d79c0759ab4ef83c9b643/tumblr_inline_o78eltLfkE1u1xff4_500.png" height={150} width={180}/>
                    </div>
                    <div className="col-md-8">
                        <label>Name:</label>
                        <p> <input type="text" onChange={this.handleChangeName} value={this.state.name || ''} className="form-control" /></p>
                        <label>Email Private:</label>
                        <p> <input type="text" onChange={this.handleChangeEmailPriv} value={this.state.emailPrivate || ''} className="form-control" /></p>
                        <label>Email University:</label>
                        <p> <input type="text" onChange={this.handleChangeEmailUni} value={this.state.emailUni || ''} className="form-control"/></p>
                        {this.state.role == "student" ? (
                            <p> Number:  {this.state.studentNumber}</p>
                        ) : (
                            <p> Departament:  {this.state.departament}</p>
                        )}
                        <button onClick={() => { this.saveEditedData() } } className="btn btn-default">Save</button>
                    </div>
                </div>
            )
        }
    }
});
