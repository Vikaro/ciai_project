/**
 * Created by Mateusz on 28.10.2016.
 */

// import YearsTable from "./Tables/YearsTable"

var Student = React.createClass( {
        getInitialState: function () {
            $.ajax({
                url: "/users/" + this.props.params.id.toString() ,
                dataType: 'json',
                cache: false,
                success: function(data) {
                    this.setState({student: data});
                    console.log(data);
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                }.bind(this)
            });
            return{
                student: {"id":0,"name":"","emailUni":"","studentNumber":"","departament":"",
                    "courses":[{"id":0,"courseNumber":0,"name":"","description":"","ects":0,"edition":0,"evaluationSteps":[],"years":""}]
                }
            //     courses:[
            //         {"id": "1", "number": "10010", "name":"CIAI", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2016-2017", "description":"This course focuses on the principles and concepts on the development of Internet applications. The syllabus follows an approach based on the fundamentals of software development based on web and service oriented architectural patterns, advanced modularity mechanisms, data persistency abstractions, good development practices, performance concerns, and validation techniques. Course lectures are accompanied by a series of practical assignments and a final project development using frameworks, languages, and programming tools for Internet Applications that ensure the safety and compliance of the solution with relation to a specification."},
            //         {"id": "2", "number": "10011", "name":"IPM", "teachers":"John Doe, Joao Pereira", "ECTS":"4", "years":"2016-2017", "description":"This course is intended to prepare the students to develop useful and usable interfaces for interactive systems. We will study the human factors in HCI. "},
            //         {"id": "3", "number": "10012", "name":"CGI", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2016-2017", "description":"The discipline focuses on the process of creation and manipulation of graphics used systematically today, not only to design and solve specific problems, but also to communicate with software or for dissemination of information among peers."},
            //         {"id": "4", "number": "10013", "name":"AA", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2015-2016", "description":"This course aims to provide an introduction to the main principles, methods, and applications of Machine Learning."},
            //         {"id": "5", "number": "10014", "name":"SS", "teachers":"John Doe, Joao Pereira", "ECTS":"5", "years":"2015-2016", "description":"In this course you will learn key principles, methods, and techniques for enforcing security in software during development and construction."}
            //
            //     ],
            //     grades:[
            //         {"id": "1001", "id": "a49701", "name" : "Test 1", "value" : 50},
            //         {"id": "1001", "id": "a49701", "name" : "Test 2", "value" : 90},
            //         {"id": "1001", "id": "a49701", "name" : "Exam", "value" : 30}
            //     ]
            };
        },
        render: function () {
            return (
                <div>
                    <div className="row">
                        <div className="col-md-10">
                            {console.log("-------")}
                            {console.log("Student")}
                            {console.log(this.state.student)}
                            {console.log("-------------")}
                            <UserDetails student_detail = {this.state.student}/>
                            {/*<h3> Hello, {this.props.params.name} </h3>
                             <Link to="student/Joan/edit" className="btn btn-primary" >Account details</Link>*/}
                        </div>
                    </div>
                    <br/><br/><br/>
                    <div className="row">
                        <div className="col-md-10">
                            {console.log("-------")}
                            {console.log("Courses")}
                            {console.log(this.state.student.courses)}

                            <CourseTable courses={this.state.student.courses} studentId={this.state.student.id} />
                            <Link to="degrees/informatics" className="btn btn-primary btn-sm btn-block">Informatics curriculum</Link>
                        </div>
                    </div>
                </div>

            )
        }
    }
);
