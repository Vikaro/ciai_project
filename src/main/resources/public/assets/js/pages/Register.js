/**
 * Created by Mateusz on 27.10.2016.
 */
var loggedUser = "";

var Register = React.createClass({

    getInitialState: function() {
        return{
            login : "",
            password:"",
            repeat:"",
        };
    },

    sendRegistrationForm() {
        if ((this.state.password).localeCompare(this.state.repeat) != 0) {
            document.getElementById("errorSpan").innerHTML="Passwords does not equal";
        } else {
            $.post("/users/register/", {
                login: this.state.login,
                password: this.state.password,
                repeat: this.state.repeat,
            })
            loggedUser = this.state.login;
            console.log("setting loggedUser " + this.state.login)
            document.getElementById("errorSpan").innerHTML="";
            location.href = "/"
        }
    },

    handleChangeLogin(event) {
        this.setState({login: event.target.value});
    },
    handleChangePassword(event) {
        this.setState({password: event.target.value});
    },
    handleChangeRepeat(event) {
        this.setState({repeat: event.target.value});
    },
    render(){
        return(
            <div>
                <div className="col-md-4"></div>
                <div className="col-md-4">
                    <span id="errorSpan"> </span>
                    <p>Login: </p>
                    <input type="text" className="form-control"  value={this.state.login} onChange={this.handleChangeLogin} />
                    <p>Password: </p>
                    <input type="password" className="form-control" value={this.state.password} onChange={this.handleChangePassword}/>
                    <p>Repeat: </p>
                    <input type="password" className="form-control" value={this.state.repeat} onChange={this.handleChangeRepeat}/>
                    <Link onClick={() => { this.sendRegistrationForm() }} className="btn btn-primary btn-lg btn-block" >Register</Link>
                </div>
                <div className="col-md-4"></div>
            </div>
        )
    }
});