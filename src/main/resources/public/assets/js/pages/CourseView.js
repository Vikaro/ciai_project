// import React from 'react';
// import FilterableTable from './Tables/StudentsTable';
// import CourseInformation from './CourseComponents/CourseInformation';
// import CourseList from './Tables/CourseList';

var CourseView = React.createClass({
    getInitialState: function() {
        return{
            students :
                [
                    {"name":"John", "age":"23", "city":"Agra", "id" : "a49701", "email": "john@example.com"},
                    {"name":"Steve", "age":"28", "city":"Delhi", "id" : "a49702", "email": "steve@example.com"},
                    {"name":"Peter", "age":"32", "city":"Chennai", "id" : "a49703", "email": "peter@example.com"},
                    {"name":"Chaitanya", "age":"28", "city":"Bangalore", "id" : "a49704", "email": "chaitanya@example.com"},
                    {"name": "Stiliyan", "age":"24", "city":"Sofia", "id" : "a49705", "email": "stiliyan@example.com"}
                ],
            courses:[
                {"id": "1", "number": "10010", "name":"CIAI", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2016-2017", "description":"This course focuses on the principles and concepts on the development of Internet applications. The syllabus follows an approach based on the fundamentals of software development based on web and service oriented architectural patterns, advanced modularity mechanisms, data persistency abstractions, good development practices, performance concerns, and validation techniques. Course lectures are accompanied by a series of practical assignments and a final project development using frameworks, languages, and programming tools for Internet Applications that ensure the safety and compliance of the solution with relation to a specification."},
                {"id": "2", "number": "10011", "name":"IPM", "teachers":"John Doe, Joao Pereira", "ECTS":"4", "years":"2016-2017", "description":"This course is intended to prepare the students to develop useful and usable interfaces for interactive systems. We will study the human factors in HCI. "},
                {"id": "3", "number": "10012", "name":"CGI", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2016-2017", "description":"The discipline focuses on the process of creation and manipulation of graphics used systematically today, not only to design and solve specific problems, but also to communicate with software or for dissemination of information among peers."},
                {"id": "4", "number": "10013", "name":"AA", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2015-2016", "description":"This course aims to provide an introduction to the main principles, methods, and applications of Machine Learning."},
                {"id": "5", "number": "10014", "name":"SS", "teachers":"John Doe, Joao Pereira", "ECTS":"5", "years":"2015-2016", "description":"In this course you will learn key principles, methods, and techniques for enforcing security in software during development and construction."}

            ],
            grades:[
                {"id": "1001", "id": "a49701", "name" : "Test 1", "value" : 50},
                {"id": "1001", "id": "a49701", "name" : "Test 2", "value" : 90},
                {"id": "1001", "id": "a49701", "name" : "Exam", "value" : 30}
            ]
        };
    },

    render(){
        return (
            <div>
                <Header/>
                <div><CourseInformation course_info = {this.state.courses[0]}/></div>
                {/*this.state.students.map((person, i) => <TableRow key = {i} data = {person} />)*/}
                {/* <div><FilterableTable students={ this.state.students} /></div>
                <div><CourseList courses={this.state.courses} /></div> */}
            </div>
        )
    }
});

var Header = React.createClass({
    getInitialState : function(){
        return {

        }
    },
    render(){
        return
        (
            <div>
                <h1>Students List</h1>
            </div>
        )
    }
});