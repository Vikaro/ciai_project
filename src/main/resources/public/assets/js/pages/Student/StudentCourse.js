/**
 * Created by Mateusz on 29.10.2016.
 */

var StudentCourse = React.createClass( {

    getInitialState: function () {
        return{
            course_info:{"id":0,"courseNumber":0,"name":" ","users":[{"id":3,"name":"aaa","emailUni":"u2@fct.com","studentNumber":"12345","departament":"1234"}],"description":"Description .....","ects":6,"edition":1,"evaluationSteps":[],"years":"2015/2016"},
            grades: [{"id":1,"grade":2,"evaluationStep":{"id":1,"name":"test1"},"user":{"id":1,"name":"Joao","emailUni":"u1@fct.com","studentNumber":"1234","departament":"123","role":"student"}}]
        }
    },
    componentDidMount: function () {
        console.log("StudentCourse");
        console.log("Componentdidmount");
        console.log(this.state.course_info);
        console.log("---");
        console.log(this.props.params.courseId);
        $.ajax({
            url: "/courses/information/" + this.props.params.courseId,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({course_info: data});
                console.log(data);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
        $.ajax({
            url: "/grades/" + this.props.params.id + "/"+ this.props.params.courseId,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({grades: data});
                console.log(data);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        })
    },

//     var courseGrades = [
//         {"id": "1001", "id": "a49701", "name" : "Test 1", "value" : 50},
//         {"id": "1001", "id": "a49701", "name" : "Test 2", "value" : 90},
//         {"id": "1001", "id": "a49701", "name" : "Exam", "value" : 30}
//     ];
//     console.log(courseInfo);
//     return{
//         courseInfo,
//         courseGrades
//     };
//
//     console.log(this.state.courseInfo);
// },
    render() {
        return (
            <div>
                <div><CourseInformation course_info = {this.state.course_info}/></div>
                <div><CourseGrades grades = {this.state.grades}/> </div>
                {/*this.state.students.map((person, i) => <TableRow key = {i} data = {person} />)*/}
            </div>
        );
    }
});
