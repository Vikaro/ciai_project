/**
 * Created by Mateusz on 28.10.2016.
 */
var Edit = React.createClass( {
    getInitialState: function() {
        return{
            adress: "Lisboa",
            email: "joan@gmail.com"
        };
    },

    handleChangeAdress(event) {
        this.setState({adress: event.target.value});
    },
    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    },

    render(){
        return (
            <div>
                <div className="col-md-4">
                    <img src="http://67.media.tumblr.com/77daf6e61a4d79c0759ab4ef83c9b643/tumblr_inline_o78eltLfkE1u1xff4_500.png" height={150} width={180}/>
                </div>
                <div className="col-md-8">
                    <p> Photo:  <input type="file" className="btn btn-primary btn-block" /></p>
                    <p> Adress:  <input type="text" className="form-control" value={this.state.adress} onChange={this.handleChangeAdress}/></p>
                    <p> Email:  <input type="text" className="form-control" value={this.state.email} onChange={this.handleChangeEmail}/></p>
                    <button className="btn btn-primary btn-block" >Save</button>
                </div>

            </div>
        )
    }
});