/**
 * Created by Mateusz on 29.10.2016.
 */

var ProfessorCourse = React.createClass( {

    getInitialState: function () {
        return{
            course_info:{"id":0,"courseNumber":0,"name":" ","users":[{"id":3,"name":"aaa","emailUni":"u2@fct.com","studentNumber":"12345","departament":"1234"}],"description":"Description .....","ects":6,"edition":1,"evaluationSteps":[],"years":"2015/2016"},
            new_evaluation_step: "",
            new_user: ""
        }
    },
    componentDidMount: function () {
        $.ajax({
            url: "/courses/information/" + this.props.params.courseId,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({course_info: data});
                console.log(data);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },

    newEval: function () {
        $.ajax({
            url: "/courses/addEval/",
            type: "POST",
            data: {
                name: this.state.new_evaluation_step.toString(),
                courseId: this.state.course_info.id.toString(),
            },
            success: function (data) {
                alert("Added new evaluation step")
                this.componentDidMount();
            }.bind(this)
        });
    },

    handleChangeName(event) {
        this.setState({new_evaluation_step: event.target.value});
    },

    render() {
        return (
            <div>
                <div><CourseInformation course_info = {this.state.course_info}/></div>
                <br />
                <h2>Evaluation steps </h2>
                <div><EvalStepsTable evals = {this.state.course_info.evaluationSteps}/></div>
                <input type="text" className="form-control"  evals={this.state.evaluationSteps} onChange={this.handleChangeName} />
                <button onClick={() => { this.newEval() }} className="btn btn-default">Add new Eval</button>
            </div>
        );
    }
});

var EvalStepsTable = React.createClass ({
    render() {
        var rows = [];
        this.props.evals.forEach((evalutaion) => {
            rows.push(<span>{evalutaion.name.toString() + "\n"}</span>)
        });
        return (
            <div>
                {rows}
            </div>
        );
    }
});
