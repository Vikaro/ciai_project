/**
 * Created by stili on 10/28/2016.
 */
// import Router from "react-router"
var CourseInformation = React.createClass({
    render(){
        console.log(this.props.course_info);
        return (
            <div>
                <CourseID name={this.props.course_info.name} number={this.props.course_info.courseNumber}/>
                <b>    {this.props.course_info.years} </b>
                <br/> <b>ECTS:</b> {this.props.course_info.ects}
                <Teacher teachers={this.props.course_info.users}/>
                <CourseDescription description = {this.props.course_info.description}/>
                <br/><br/>
            </div>
        )
    }
});

var CourseID = React.createClass( {
    render() {
        return (
            <div>
                <h1>{this.props.name}({this.props.number} )</h1>
            </div>
        );
    }
});

var Teacher =  React.createClass ({
    render() {
        var rows = [];
        this.props.teachers.forEach((el) => {
            rows.push(el.name);
        });
        return (
            <div>
                <p><b>Teachers:</b> {rows}</p>
            </div>
        );
    }
});


var CourseDescription = React.createClass( {
    render() {
        return (
            <div>
                <h3>Short course description: <br/> </h3>
                {this.props.description}
            </div>
        );
    }
});