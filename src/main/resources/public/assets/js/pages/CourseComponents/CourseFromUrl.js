/**
 * Created by stili on 10/29/2016.
 */
/**
 * Created by stili on 10/29/2016.
 */
/**
 * Created by Mateusz on 29.10.2016.
 */
var CourseFromUrl = React.createClass ({

    getInitialState: function() {
            var idCourse = this.props.params.number;
        console.log(idCourse);
        var courses = [
            {"id": "1", "number": "10010", "name":"CIAI", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2016-2017", "description":"This course focuses on the principles and concepts on the development of Internet applications. The syllabus follows an approach based on the fundamentals of software development based on web and service oriented architectural patterns, advanced modularity mechanisms, data persistency abstractions, good development practices, performance concerns, and validation techniques. Course lectures are accompanied by a series of practical assignments and a final project development using frameworks, languages, and programming tools for Internet Applications that ensure the safety and compliance of the solution with relation to a specification."},
            {"id": "2", "number": "10011", "name":"IPM", "teachers":"John Doe, Joao Pereira", "ECTS":"4", "years":"2016-2017", "description":"This course is intended to prepare the students to develop useful and usable interfaces for interactive systems. We will study the human factors in HCI. "},
            {"id": "3", "number": "10012", "name":"CGI", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2016-2017", "description":"The discipline focuses on the process of creation and manipulation of graphics used systematically today, not only to design and solve specific problems, but also to communicate with software or for dissemination of information among peers."},
            {"id": "4", "number": "10013", "name":"AA", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2015-2016", "description":"This course aims to provide an introduction to the main principles, methods, and applications of Machine Learning."},
            {"id": "5", "number": "10014", "name":"SS", "teachers":"John Doe, Joao Pereira", "ECTS":"5", "years":"2015-2016", "description":"In this course you will learn key principles, methods, and techniques for enforcing security in software during development and construction."}

        ];
        var courseInfo = courses.filter(function(el){
            return el.number === idCourse;
        });
        console.log(courseInfo);
        return{
            courseInfo: courseInfo
        };
    },
    render() {
        return (
            <div>
                <div><CourseInformation course_info = {this.state.courseInfo[0]}/></div>
                <div> </div>
                {/*this.state.students.map((person, i) => <TableRow key = {i} data = {person} />)*/}
            </div>
        );
    }
});


