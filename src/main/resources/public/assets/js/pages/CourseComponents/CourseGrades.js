/**
 * Created by Mateusz on 29.10.2016.
 */
var CourseGrades = React.createClass({
    render() {
        var rows = [];
        var rk = 0; // row key
        var average = 0;
        this.props.grades.forEach((el) => {
            // id.courses.forEach(function(courseId) {
            average += el.grade;
            rows.push(<CourseRow name={el.evaluationStep.name} grade={el.grade} key={rk++} />);
            {/*rows.push(<CourseRow name={el.evaluationStep.name} value = {el.value} key={rk++} />);*/}

        });

        average /= rk;
        return (
            <div>
                <table className="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Test: </th>
                        <th>Value: </th>
                    </tr>
                    </thead>
                    <tbody>
                    {rows}
                    </tbody>
                </table>

                <b>Average grade: <h4>{average}</h4> </b>
            </div>
        )
    }
});

var CourseRow = React.createClass ({
    render() {
        return (
                <tr>
                    <td>{this.props.name}</td>
                    <td>{this.props.grade}</td>
                </tr>
        );
    }
});
