/**
 * Created by Mateusz on 28.10.2016.
 */
var Courses = React.createClass( {

    getInitialState: function() {
        return {
            addNew : false,
            coursesList : [
                {
                    name: "CGI",
                    number: "CG0666",
                    description: "Lorem ipsum",
                    ects: 6,
                    edition: "fall",
                    teaching_team: ["Teacher1", "Teacher2", "Teacher3"]
                },
                {
                    name: "IPM",
                    number: "IP6660",
                    description: "IPM description",
                    ects: 6,
                    edition: "fall",
                    teaching_team: ["Teacher1", "Teacher2", "Teacher3"]
                }
            ]
        };
    },

    render(){
        return (
            <div>
                {
                    this.state.coursesList.map(function (element, i) {
                        return (
                            <Course>{element.name}</Course>);
                            <button>Edit</button>
                    })
                }

            </div>
        )
    }
});

var Course= React.createClass({

    render(){
        return (
            <div>
                {this.props.children}
            </div>
        )
    }
});