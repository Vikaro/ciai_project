/**
 * Created by stili on 10/29/2016.
 */

var Degree = React.createClass ({

    getInitialState: function() {
        var courses_list = [
                {"number": "10010", "name":"CIAI", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2016-2017", "semester": "I","description":"This course focuses on the principles and concepts on the development of Internet applications. The syllabus follows an approach based on the fundamentals of software development based on web and service oriented architectural patterns, advanced modularity mechanisms, data persistency abstractions, good development practices, performance concerns, and validation techniques. Course lectures are accompanied by a series of practical assignments and a final project development using frameworks, languages, and programming tools for Internet Applications that ensure the safety and compliance of the solution with relation to a specification."},
                {"number": "10011", "name":"IPM", "teachers":"John Doe, Joao Pereira", "ECTS":"4", "years":"2016-2017", "semester": "I", "description":"This course is intended to prepare the students to develop useful and usable interfaces for interactive systems. We will study the human factors in HCI. "},
                {"number": "10012", "name":"CGI", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2016-2017", "semester": "II", "description":"The discipline focuses on the process of creation and manipulation of graphics used systematically today, not only to design and solve specific problems, but also to communicate with software or for dissemination of information among peers."},
                {"number": "10013", "name":"AA", "teachers":"John Doe, Joao Pereira", "ECTS":"6", "years":"2015-2016", "semester": "II", "description":"This course aims to provide an introduction to the main principles, methods, and applications of Machine Learning."},
                {"number": "10014", "name":"SS", "teachers":"John Doe, Joao Pereira", "ECTS":"5", "years":"2015-2016", "semester": "III", "description":"In this course you will learn key principles, methods, and techniques for enforcing security in software during development and construction."}

            ];
        console.log(this.props.params.name);
      return{
          courses : courses_list,
      }
    },
    render() {
        return (
            <div>
                {/*<Header name={this.props.params.name}/>*/}
                <div><DegreeTable courses={this.state.courses} /></div>
                <div>...</div>
                <div>Thesis: 60 <br></br>
                    Total ECTS: 240</div>
            </div>
        );
    }
});

var Header = React.createClass ({
    render() {
        return (
            <div>
                <h1>{this.props.name.toUpperCase()}</h1>
            </div>
        );
    }
});