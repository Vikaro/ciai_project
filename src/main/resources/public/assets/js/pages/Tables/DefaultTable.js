// with es6
import React from 'react';
//import ReactBsTable from 'react-bootstrap-table';
var ReactBsTable  = require('react-bootstrap-table');
var BootstrapTable = ReactBsTable.BootstrapTable;
var TableHeaderColumn = ReactBsTable.TableHeaderColumn;
var students_list = require("json!../../../students.json");
        const products = [];

        function addProducts(quantity) {
            const startId = products.length;
            for (let i = 0; i < quantity; i++) {
                const id = startId + i;
                products.push({
                    id: id,
                    name: 'Item name ' + id,
                    price: 2100 + i
                });
            }
        }
addProducts(5);

export default class DefaultTable extends React.Component {
    render() {
        return (
            <div>
            <BootstrapTable data={ students_list.students }>
                <TableHeaderColumn dataField='id' isKey={ true }>ID</TableHeaderColumn>
                <TableHeaderColumn dataField='name' filter={ { type: 'TextFilter', delay: 100 }  }>Name</TableHeaderColumn>
                <TableHeaderColumn dataField='age'>Age</TableHeaderColumn>
                <TableHeaderColumn dataField='city'>City</TableHeaderColumn>
                <TableHeaderColumn dataField='email'>Email</TableHeaderColumn>

            </BootstrapTable>

            <BootstrapTable data={ students_list.courses }>
            <TableHeaderColumn dataField='id' isKey={ true }>ID</TableHeaderColumn>
            <TableHeaderColumn dataField='name' filter={ { type: 'TextFilter', delay: 100 }  }>Name</TableHeaderColumn>
            <TableHeaderColumn dataField='number'>Number</TableHeaderColumn>
            <TableHeaderColumn dataField='teachers'>Theachers:</TableHeaderColumn>
            <TableHeaderColumn dataField='years'>Years</TableHeaderColumn>

            </BootstrapTable>
                </div>
        );
    }
}
