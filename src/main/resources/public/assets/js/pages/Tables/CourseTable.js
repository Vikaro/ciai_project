/**
 * Created by Mateusz on 28.10.2016.
 */
/**
 * Created by Mateusz on 28.10.2016.
 */

var YearRow = React.createClass( {
    render() {
        return (
            <tr><th colSpan="2">{this.props.year}</th></tr>
        );
    }
});

var CourseTableRow = React.createClass( {
    render() {
        var name = this.props.name;
        var id = this.props.id;
        var  currentRoute = "/student/" + this.props.studentId +"/course/";
        // here321
        var link = encodeURI(currentRoute + id + "/");
        return (
            <tr>
                <td><Link to={link}> {name} </Link></td>
            </tr>
        );
    }
});

var CourseRowProfessor = React.createClass( {
    render() {
        var name = this.props.name;
        var id = this.props.id;
        var  currentRoute = "/professor/" + this.props.studentId +"/course/";
        // here321
        var link = encodeURI(currentRoute + id + "/");
        return (
            <tr>
                <td><Link to={link}> {name} </Link></td>
            </tr>
        );
    }
});


var CourseTable = React.createClass( {

    render(){
        var rows = [];
        var lastYear = null;
        var rk = 0; // row key
        this.props.courses.forEach((id) => {

            // if (course.name.indexOf(this.props.filterText) === -1) {
            //     return;
            // }
            if (id.years !== lastYear) {
                rows.push(<YearRow year={id.years} key={rk++} />);
            }
            // id.courses.forEach(function(courseId) {
            rows.push(<CourseTableRow id={id.courseNumber} name={id.name} studentId={this.props.studentId} key={rk++}  />);

            // });
            lastYear = id.years;
        });
        return (
            <div>
                <table className="table table-striped table-hover">
                    <thead>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
            </div>
        )
    }
});


var CourseTableProfessor = React.createClass( {

    render(){
        var rows = [];
        var lastYear = null;
        var rk = 0; // row key
        this.props.courses.forEach((id) => {

            // if (course.name.indexOf(this.props.filterText) === -1) {
            //     return;
            // }
            if (id.years !== lastYear) {
                rows.push(<YearRow year={id.years} key={rk++} />);
            }
            // id.courses.forEach(function(courseId) {
            rows.push(<CourseRowProfessor id={id.courseNumber} name={id.name} studentId={this.props.studentId} key={rk++}  />);

            // });
            lastYear = id.years;
        });
        return (
            <div>
                <table className="table table-striped table-hover">
                    <thead>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
            </div>
        )
    }
});