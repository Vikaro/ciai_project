/**
 * Created by stili on 10/29/2016.
 */
var SearchBox = React.createClass ({
    getInitialState: function() {

    },

    handleChange() {
        this.props.onUserInput(
            this.refs.filterTextInput.value,
        );
    },

    render() {
        return (
            <form>
                <input className="form-control"
                    type="text"
                    placeholder="Search..."
                    value={this.props.filterText}
                    ref="filterTextInput"
                    onChange={this.handleChange}
                />
            </form>
        );
    }
});