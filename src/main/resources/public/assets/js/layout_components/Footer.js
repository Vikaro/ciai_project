/**
 * Created by Mateusz on 27.10.2016.
 */


var Footer = React.createClass({
    render() {
        return (
            <footer>
                <div className="row">
                    <div className="col-lg-12">
                        <p>Copyright &copy; ciai-project</p>
                    </div>
                </div>
            </footer>
        );
    }
}
);